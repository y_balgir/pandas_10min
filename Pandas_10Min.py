import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

s = pd.Series([1,3,5,np.nan,6,8])
#print(s)

dates = pd.date_range('20130101',periods=6)
#print(dates)

df=pd.DataFrame(np.random.randn(6,4),index=dates,columns=list('ABCD'))
#print(df)


df2 = pd.DataFrame({ 'A' : 1.,
'B' : pd.Timestamp('20130102'),
'C' : pd.Series(1,index=list(range(4)),dtype='float32'),
'D' : np.array([3] * 4,dtype='int32'),
'E' : pd.Categorical(["test","train","test","train"]),
'F' : 'foo' })

#print(df2)
#print(df2.dtypes)

#print("\n df.head")
#print(df.head)

#print("\n df.index")
#print(df.index)

#print("\n df.describe")
#print(df.describe())

#print("\n df.sort_index(axis=0, ascending=False)")
#print(df.sort_index(axis=0, ascending=False))

#print("\n df.sort_index(axis=0, ascending=False)")
#print(df.sort_index(axis=0, ascending=False))

print("\n df.sort_values(by='B', ascending=False)")
print(df.sort_values(by='B', ascending=False))





